
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity gradientUnit is
    Port ( I_Pix11, I_Pix12, I_Pix13 : in STD_LOGIC_VECTOR (7 downto 0);
		   I_Pix21, I_Pix22, I_Pix23 : in STD_LOGIC_VECTOR (7 downto 0);
		   I_Pix31, I_Pix32, I_Pix33 : in STD_LOGIC_VECTOR (7 downto 0);
		   O_pixEdge : out  STD_LOGIC		   
		   ); 
end gradientUnit;


architecture Behavioral of gradientUnit is

-- déclaration des signaux internes
signal Gx: signed(10 downto 0) := (others => '0');
signal Gy: signed(10 downto 0) := (others => '0');
	
constant threshold: integer := 99; 

begin

	process(I_Pix11, I_Pix12, I_Pix13, I_Pix21, I_Pix22, I_Pix23, I_Pix31, I_Pix32, I_Pix33)
	begin
	
	Gy <= signed("000"&I_Pix11) + signed('0'&unsigned(I_Pix12)&'0') + signed("000"&I_Pix13) - signed("000"&I_Pix31) - signed('0'&I_Pix32&'0') - signed("000"&I_Pix33);
	Gx <= - signed("000"&I_Pix11) + signed("000"&I_Pix13) - signed('0'&(I_Pix21)&'0') + signed('0'&(I_Pix23)&'0') - signed("000"&I_Pix31) + signed("000"&I_Pix33);
    	
    if ((abs(Gx) + abs(Gy)) >= threshold) then
        O_pixEdge <= '1';
    else 
        O_pixEdge <= '0';
    end if;
	
	end process;
		

end Behavioral;

