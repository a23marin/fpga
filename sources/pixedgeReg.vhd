
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity pixedgeReg is
    Port ( clk			:in STD_LOGIC;
		   reset		:in STD_LOGIC;
		   I_pixEdge : in  STD_LOGIC;
		   I_ldPixEdge : in STD_LOGIC;
		   O_pixEdge : out  STD_LOGIC		   
		   ); 
end pixedgeReg;


architecture Behavioral of pixedgeReg is

-- déclaration des signaux internes
    signal memoizedPixEdge: STD_LOGIC := '0';

begin
    process(clk, reset, I_pixEdge, I_ldPixEdge)
    begin
	
    if (RISING_EDGE(clk)) then
        if (reset = '1') then
	       memoizedPixEdge <= '0';
       elsif (I_ldPixEdge = '1') then
           memoizedPixEdge <= I_pixEdge;
        end if;
	end if;
--	_BLANK_
    end process;
    
    O_pixEdge <= memoizedPixEdge;
 
end Behavioral;

